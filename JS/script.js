const positionNotes = () => {
  document.querySelectorAll(`a[id^="fnref:"]`).forEach((element, index) => {
    document
      .querySelector(`li[id^="fn:${index}"]`)
      .setAttribute(
        'style',
        `top: ${element.getBoundingClientRect().top +
          window.pageYOffset -
          document.documentElement.clientTop}px`
      )
  })
}

document.addEventListener('DOMContentLoaded', () => {
  window.addEventListener('resize', positionNotes)
  document.querySelector('body').classList.add('stomped')
  positionNotes()
})
